FROM ubuntu:xenial

RUN apt-get update -y && apt-get install -y \
    apt-utils \
    bzip2 \
    gcc \
    make \
    ncurses-dev \
    wget \
    zlib1g-dev \
    openjdk-8-jdk \
    graphviz

#################
# Samtools 1.12 #
#################
RUN wget https://github.com/samtools/samtools/releases/download/1.12/samtools-1.12.tar.bz2 && \
    tar -xjf samtools-1.12.tar.bz2 && \
    rm samtools-1.12.tar.bz2 && \
    cd samtools-1.12 && \
    ./configure && \
    make && \
    make install

#######
# BWA #
#######
RUN wget https://github.com/lh3/bwa/releases/download/v0.7.17/bwa-0.7.17.tar.bz2 && \
    tar -xjf bwa-0.7.17.tar.bz2 && \
    rm bwa-0.7.17.tar.bz2 && \
    cd bwa-0.7.17 && \
    make && \
    cp bwa /usr/local/bin

###########
# Varscan #
###########
RUN mkdir varscan && \
    cd varscan && \
    wget https://anaconda.org/bioconda/varscan/2.4.4/download/noarch/varscan-2.4.4-0.tar.bz2 && \
    tar -xjf varscan-2.4.4-0.tar.bz2 && \
    rm varscan-2.4.4-0.tar.bz2 && \
    cp share/varscan-2.4.4-0/VarScan.jar  share/varscan-2.4.4-0/varscan /usr/local/bin && \
    cd /

##############
# R Packages #
##############
RUN R -e "install.packages('data.table', dependencies=TRUE, repos='http://cran.rstudio.com/')"