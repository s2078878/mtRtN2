#!/usr/bin/env nextflow

log.info """\
         =======================================================
         M T R T N :   M T D N A   V A R I A N T   C A L L I N G
         =======================================================
         parameters_file:   ${params.parameters}
         purity_file:       ${params.purity}
         out_directory:     ${params.outdir}
         matched_samples:   ${params.matched}
         rna-seq:           ${params.rnaseq}
         =======================================================
         """
         .stripIndent()

/*
 * Input parameters validation
 */
/// Parameters file
if (params.parameters){
    
    parameters_file = file(params.parameters)
        
    Channel
        .fromPath(params.parameters)
        .splitCsv(header:true)
        .map{ row -> tuple(row.row, row.sampleID, row.group, row.bam, row.index)}
        .set{ parameters_ch }

    if( !parameters_file.exists() ) exit 1, "ERROR: Parameters file doesn't exist: ${parameters_file}"

} else {
    
    exit 1, "ERROR: Missing parameters file. Please set a parameters file using the --parameters flag."

}

/// Purity file
if (params.purity){
    
    purity_file = file(params.purity)

    Channel
        .fromPath(params.purity)
        .splitCsv(header:true)
        .map{ row -> tuple(row.sampleID, row.group, row.purity) }
        .set{ purity_ch }
    
    if( !purity_file.exists() ) exit 1, "Purity file doesn't exist: ${purity_file}"

} else {
    
    purity_file = "false"

}

/// CRAM Reference
if (params.cram_reference){
    
    cram_reference_file = file(params.cram_reference)
    
    Channel
        .fromPath(params.cram_reference)
        .set{cram_reference_ch}
    
    if( !cram_reference_file.exists() ) exit 1, "CRAM Reference file doesn't exist: ${cram_reference_file}"

} else {
    
    cram_reference_file = "false"

}

Channel
    .fromPath(params.reference)
    .set{reference_ch}

Channel
    .fromPath(params.numts)
    .set{numts_ch}

/*
 * Extract only the mitochondria reads from the bam files
 */
process chrm {
    tag "Pulling chrM from bam file"

    input:
    set row, sampleID, group, path(bam), path(index) from parameters_ch
    path cram_reference from cram_reference_ch.collect()

    output:
    set sampleID, group, path("${sampleID}.${group}.bam"), path("${sampleID}.${group}.bam.bai") into mitochondria_ch

    script:
    """
    if [ "${cram_reference_file}" == "false" ]; then
        samtools view -h -b ${bam} chrM MT M chrMT > ${sampleID}.${group}.bam
    else
        samtools view -h -b -T ${cram_reference} ${bam} chrM MT M chrMT > ${sampleID}.${group}.bam
    fi

    samtools index ${sampleID}.${group}.bam
    """
}

/*
 * Run RtN on chrM bam file to remove NUMT reads
 */
process rtn {
    tag "Removing NUMT reads"
    label "bigmem"

    input:
    set sampleID, group, path(chrM_bam), path(index) from mitochondria_ch
    path reference from reference_ch.collect()
    path numts from numts_ch.collect()

    output:
    set sampleID, group, path("${sampleID}.${group}.rtn.bam") into filt_ch

    script:
    if( "${params.rnaseq}" == "false" )
        """
        CHRNAME=(\$(samtools idxstats ${chrM_bam} | sort -k3,3 | tail -1 | cut -f1))
        rtn -p -h humans.fa -n Calabrese_Dayama_Smart_Numts.fa -c \$CHRNAME -b ${chrM_bam}
        """
    else if( "${params.rnaseq}" == "true" )
        """
        mv ${chrM_bam} ${sampleID}.${group}.rtn.bam
        """
    else
        exit 1, "ERROR: Invalid rnaseq mode: ${params.rnaseq}\nCheck that --rnaseq is set to true or false."
}

/*
 * Sort the files and create mpileup file ready for varscan
 */
process pileup {
    tag "Sorting and creating pileup files"

    input:
    set sampleID, group, path(filtered_chrM) from filt_ch

    output:
    set sampleID, group, path("${sampleID}.${group}.pileup") into mpileup_ch

    script:
    """
    samtools sort ${filtered_chrM} | \
    samtools mpileup -d 0 -f $baseDir/data/ref.fa - > ${sampleID}.${group}.pileup
    """
}

if(purity_file != "false" && "${params.matched}" == "true"){

    // Merge the pileup files and the purity data
    mpileup_ch
        .mix(purity_ch)
        .groupTuple(by:[0,1]) 
        .map { input -> tuple(input[0], input[2][0], input[2][1]) }
        .groupTuple(by:0)
        .map { input -> tuple(input[0], input[1][1], input[1][0], input[2][1], input[2][0]) }
        .set { merged_pileup_ch }

} else if(purity_file == "false" && "${params.matched}" == "true") {

    mpileup_ch
        .groupTuple(by:0)
        .map { sampleID, groups, files -> tuple( sampleID, groups, files.sort{it.name} ) }
        .map { input -> tuple(input[0], 1, 1, input[2][0], input[2][1]) }
        .set { merged_pileup_ch }

} else {

    mpileup_ch
        .map { input -> tuple(input[0], "EMPTY", "EMPTY", input[2], "$baseDir/data/ref.fa") }
        .set { merged_pileup_ch }

}

/*
 * Run VarScan2 to call mitochondrial DNA variants and if they are germline/somatic
 */
process varscan {
    tag "Running VarScan2"

    input:
    set sampleID, n_purity, t_purity, path(n_pileup), path(t_pileup) from merged_pileup_ch

    output:
    set path("${sampleID}.snp"), path("${sampleID}.indel") into var_ch

    script:
    if( "${params.matched}" == "true" )
        """
        varscan somatic ${n_pileup} ${t_pileup} ${sampleID} \
        --strand-filter 1 --min-avg-qual 30 --min-coverage 2 --min-reads 2 --min-var-freq 0 \
        --normal-purity ${n_purity} --tumor-purity ${t_purity}
        """
    else if( "${params.matched}" == "false" )
        """
        varscan mpileup2snp ${n_pileup} \
            --strand-filter 1 --min-avg-qual 30 \
            --min-coverage 2 --min-reads2 2 --min-var-freq 0 > ${sampleID}.snp

        varscan mpileup2indel ${n_pileup} \
            --strand-filter 1 --min-avg-qual 30 \
            --min-coverage 2 --min-reads2 2 --min-var-freq 0 > ${sampleID}.indel
        """
    else
        exit 1, "ERROR: Invalid matched-sample mode: ${params.matched}\nCheck that --matched is set to true or false."
}

/*
 * Merge all variant calls
 */
process merge {
    tag "Merging variant calls"
    publishDir "${params.outdir}/results/" , mode:'copy'

    input:
    path(var_calls) from var_ch.collect()

    output:
    set path("combinedSNPs.txt"), path("combinedIndels.txt") into combined_ch

    script:
    """
    mergeCalls.R ${var_calls}
    """
}

workflow.onComplete {
	log.info ( workflow.success ? "\nDone! All mitochondrial DNA variants called.\n" : "\nOops .. something went wrong.\n" )
}